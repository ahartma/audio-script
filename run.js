"use strict";
var express = require('express');
var bodyParser = require('body-parser')
var spawn = require('child_process').spawn;
var fs = require('fs');

/**
 * A mapping from user voice names to internal use festival names.
 * @type {{Male1: string, Male2: string, Female1: string, Female2: string, Male3: string, Male4: string}}
 */
var Voices  = {
    "Male1" : "cmu_us_bdl_arctic_clunits",
    "Male2" : "cmu_us_rms_arctic_clunits",
    "Female1" : "cmu_us_clb_arctic_clunits",
    "Female2" : "cmu_us_slt_arctic_clunits",
    "Male3" : "cmu_us_jmk_arctic_clunits",
    "Male4" : "cmu_us_awb_arctic_clunits"
};

/**
 * Convert supplied text to an audio file and return on success.
 * @param text The text to convert to audio
 * @param voice The accent voice to use for the audio
 * @param res The result object to resolve on completion or error
 * @param id The uuid of the audio.
 */
var textToWave = function(text, voice, res, id) {
    fs.writeFile(`tmp/${id}.txt`, text, function(err) {
        if(err) {
            console.log(`stderr: ${error}`);
            // We failed to write the temporary .txt file
            res.sendStatus(400);
        }

        console.log(`tmp/${id}.txt successfully created.`);

        // Request a text-to-audio conversion using the temporary audio.txt file
        var tts = spawn('text2wave', ['-o', `tmp/${id}.wav`, `tmp/${id}.txt`, '-eval', `(voice_${Voices[voice]})`]);

        tts.on('error', (error) => {
            // Conversion failed.
            console.log(`stderr: ${error}`);
            res.sendStatus(400);
        });

        tts.on('close', (code) => {
            if(code == 0) {
                // Conversion succeeded.
                console.log(`tmp/${id}.wav successfully created.`);
                res.json({});
            } else {
                // Conversion failed
                console.log(`stderr: text2wave exited with code ${code}`);
                res.sendStatus(400);
            }
        });
    });
};

/**
 * Run the audioscript server application
 * @param argv
 */
var runApplication = function(argv) {
    var app = express();
    var jsonParser = bodyParser.json();
    app.post('/tts', jsonParser, function (req, res) {
        if (!req.body)  {
            return res.sendStatus(400);
        }
        if(!req.body.id || !req.body.voice || !req.body.text) {
            return res.sendStatus(400);
        }

        console.log(`Received tts request : ${req.body.id}`);
        textToWave(req.body.text, req.body.voice, res, req.body.id);
    });

    app.get('/files', jsonParser, function(req, res) {
        var folder = 'assets/audio';
        if(req.body && req.body.folder) {
            folder = req.body.folder;
        }
        var files = fs.readdirSync('./' + folder);
        var fullyQualifiedFiles = [];
        for(var file of files) {
            fullyQualifiedFiles.push('/' + folder + '/' + file);
        }
        res.json({files: fullyQualifiedFiles});
    });

    var port =  argv[2];
    console.log("Opening http://localhost:" + port);
    app.use(express.static(__dirname));
    console.log("Listening on http://localhost:" + port);
    app.listen(port);
};

// Run the application
runApplication(process.argv);

