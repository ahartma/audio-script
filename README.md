# Audio Script #

## Documentation Links ##
[5.1 Pre-Proposal](https://docs.google.com/a/uw.edu/document/d/1Qb_wW7YfaSAqi-ei3biodcHTQrPYXRWiTaTO6PCWg5o/edit?usp=sharing "Pre-Proposal")

[5.2 Proposal](https://docs.google.com/a/uw.edu/document/d/1-80w3NKSIQBgS0mgNMcVABe4rpu--Ovur3c_o4r7K_Q/edit?usp=sharing "Proposal")

[5.3 Implementation](https://docs.google.com/document/d/13TBHDod73XOWHdGwZflXhZsJTjm1F6fmI_hACX4DDUQ/edit?usp=sharing)

[5.4 Report](https://docs.google.com/document/d/1u082CjfMUHVrEHRQmIxTWrI86K17zof8FLK0pp-QtTs/edit?usp=sharing)

[5.5 Poster](https://1drv.ms/p/s!Arftth26OZyQh4Fbyh_iCKX4W3K_nQ)

[Language Demonstration Screencast](https://vid.me/2AKs "Demo")

## How do I get set up? ##
AudioScript requires an Unbuntu enviornment > 12.04.

From an Unbuntu enviornment, installation of AudioScript is very simple.

- Retrieve the repository and install node
```bash
git clone https://bitbucket.org/audioscript/audio-script.git
apt-get install node
```

- Install all dependencies
This will update node, some npm packages, and festival with the CMU voice packages.
Total installation is approximately **900MB** and can take upwards of an hour.
```bash
cd audio-script
sudo npm install --unsafe-perm
```

- Start the application
```bash
npm start
```
This application will now be running at http://localhost:8888.
If you wish to run on a different port than *8888* then call the run script directily instead of using npm.

```bash
node run.js {{port}}
```


## Usage ##

AudioScript supports playing audio in two types of ways, either as text-to-speach for
named actors or through explicit .wav files relative to the application root.

### Play ###
The play command is the most basic command and begins playing a defined .wav file after
the entire script has been parsed and loaded. For example,
```bash
PLAY applause /assets/audio/applause.wav
```
tells audio-script that you wish to play the audio file at */assets/audio/applause.wav*

A starting set of audio effect files have already been added under the */assets/audio* director
and are listed under the **Insert Play** dropdown menu. To add more files simply
add them to any directory under the project root.

### Wait For ###

The Wait for command tells audio script to wait for the named audio to finish playing before continueing.
Exapanding on the above example,

```bash
PLAY applause /assets/audio/applause.wav
WAIT FOR applause
PLAY cat /assets/audio/cat.wav
```

The applause audio will now complete playing before all subsequent calls, in particular the cat audio. It is important
to note that there is no implicit wait at the end of the script. If you don't call wait on an audio file
and the script reaches the end it will be prematurely terminated. So cat will not play.

### With ###
There are 3 modifications that can be applied to audio with the **WITH** command and chained together with the **AND** command.
These modifications are

- **LOOP** : Loop the audio until a wait is requested
- **VOLUME** : Play the audio at a modified volume between [0,1].
- **RATE** : Play the audio at a rate between (0, 4]

For example,
```bash
PLAY applause /assets/audio/applause.wav
    WITH LOOP AND RATE 2 AND VOLUME .8
PLAY cat /assets/audio/cat.wav
WAIT FOR cat
WAIT FOR applause
```

Will play the applause on a loop at 2x speed with 80% of volume until the cat audio
has completed playing. At that point the applause will complete its current loop and
terminate.

### Actors ###
Actors can be defined anywhere in the script. After they are defined you can request
a text-to-speach audio be played.
To declare an actor you need to define a name and select a voice.
There are currently 6 voices available:

- Male1
- Male2
- Male3
- Male4
- Female1
- Female2

For example,
```bash
DECLARE ACTOR myActor Male1
```
defines a new actor named myActor with voice Male1. Once an actor is defined text-to-speach
can be used with the sample syntax as audio files but instead of files paths you can
use quoted text.

```bash
PLAY myActor "Words I will say!" WITH RATE 2 AND VOLUME .8 WAIT FOR myActor
```

That will cause the *"Words I will say!"* to be converted to audio with the Male1 voice and
play it back at 2x speed and .8 volume.

## Examples ##

### Simple playing of an audio file ###

```bash
#Play the audio file located at /assets/audio/applause.wav#
PLAY applause /assets/audio/applause.wav
#Wait for the applause to finish. If you don't wait than it will be immediately terminated#
WAIT FOR applause
```

### Playing of an audio file with modifications ##

```bash
#Play the audio file located at /assets/audio/applause.wav#
PLAY applause /assets/audio/applause.wav
WITH VOLUME .5 AND RATE 2 #Play the applause at half volume and double speed#
#Wait for the applause to finish. If you don't wait than it will be immediately terminated#
WAIT FOR applause
```

### A very simple actor example ###

Actors in audio script must be defined with a **DEFINE ACTOR** tag prior to their use. However, once an
actor is defined, they can freely speak using text-to-speach.
```bash
Define Actor Jill Female2                           #Define an actor named Jill with the Female2 voice#
Define Actor Jack Male2                             #Define an actor named Jack with the Male2 voice#

PLAY doorbell /assets/audio/doorbell.wav            #Play the audio file located at /assets/audio/doorbell.wav#
WAIT FOR doorbell                                   #Wait for the doorbell to finish playing#
SPEAK Jill "Is somebody at the door?"
WAIT FOR Jill                                       #Have Jill speak and wait for her to finish before continueing#
SPEAK Jack "Yes, I'll go get it."
WAIT FOR Jack                                       #Have Jack respond and wait for him to finish speaking before terminating the scene#

```

### Looped background audio example ###
Audio can be looped in the background while other assets play, such as actors or other audio scripts.
```bash
#Begin playing the applause on a loop#
PLAY applause /assets/audio/applause.wav WITH LOOP AND VOLUME .1

#Define our actor, it doesn't matter where this happens as long as its before his first speak statement#
Define Actor Jack Male2

#Tell Jack to start speaking#
SPEAK Jack "The applause just doesn't stop while I am talking does it!
How cool is that!"

#Wait for Jack to finish speaking. Only then stop the appluase loop and wait for it to terminate its last cycle.#
WAIT FOR Jack
WAIT FOR applause
```

## A Complex Example ##
```bash
Define Actor Jack Male2
Define Actor Jill Female2

# Loop a slow and quite heartbeat #
PLAY heartbeat_slow /assets/audio/heartbeat.wav
    WITH VOLUME .4 AND LOOP

# Play a creaking nose and wait for it to finish #
PLAY creak /assets/audio/creak.wav
    WITH VOLUME .6
WAIT FOR creak

# Jack and Jill have a conversation #
SPEAK Jack "Did you hear that?" WAIT FOR Jack
SPEAK Jill "No, I didn't hear anything." WAIT FOR Jill

# Play a bear growl entirely #
PLAY bear_growl /assets/audio/bear_growl.wav WAIT FOR bear_growl

# Stop our slow heartbeat and start a new faster one#
WAIT FOR heartbeat_slow
PLAY heartbeat_fast /assets/audio/heartbeat.wav
    WITH VOLUME .8 AND LOOP AND RATE 1.6


# Play gunshots, glass shattering and an explosion simultaneously #
PLAY gunshots /assets/audio/gunshots.wav
    WITH LOOP AND VOLUME .2 AND RATE 2
PLAY glass_shatter /assets/audio/glass_shatter.wav
    WITH VOLUME .7
PLAY explosion /assets/audio/explosion.wav
    WITH VOLUME .8
WAIT FOR explosion
WAIT FOR glass_shatter
WAIT FOR gunshots

# Jack and Jill have another converstation #
SPEAK Jack "What is going on in here!"
    WITH RATE 1.2
WAIT FOR Jack
SPEAK Jill "I don't know!"
    WITH RATE 1.2
WAIT FOR Jill

# The heartbeat_fast stops implicitly#
# WAIT FOR heartbeat_fast#
```


## Who do I talk to? ##

Carl Ross <cross95@uw.edu>

Andrew Hartman <hartma3@uw.edu>