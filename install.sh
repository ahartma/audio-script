#! /bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

# Install Node
apt-get update -y
apt-get install -y node
npm install -y

apt-get install wget -y

# Install Festival
apt-get install -y festival festlex-cmu festlex-poslex festlex-oald libestools1.2 unzip

# Install CMU Arctic Voices (These are very large but the highest quality)
mkdir cmu_tmp
cd cmu_tmp/
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_awb_arctic-0.90-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_bdl_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_clb_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_jmk_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_rms_arctic-0.95-release.tar.bz2
wget -c http://www.speech.cs.cmu.edu/cmu_arctic/packed/cmu_us_slt_arctic-0.95-release.tar.bz2

# Unpack all Arctic Voices
for t in `ls cmu_*` ; do tar xf $t ; done
rm *.bz2

# Install Arctic Voices to Festival
mkdir -p /usr/share/festival/voices/english/
mv * /usr/share/festival/voices/english/


for d in `ls /usr/share/festival/voices/english` ; do
if [[ "$d" =~ "cmu_us_" ]] ; then
mv "/usr/share/festival/voices/english/${d}" "/usr/share/festival/voices/english/${d}_clunits"
fi ; done

# Remove temporary files
cd ../
rm -rf cmu_tmp/

mkdir tmp


