lexer grammar AudioScriptLexer;

options { language=JavaScript; }

COMMENT : '#'  -> mode(Comment);

WS : [ \t\n\r]+ -> channel(HIDDEN) ;

PROD_COMMAND : 'PLAY' | 'play' | 'Play' | 'SPEAK' | 'speak' | 'Speak' ;
DEFINE : 'DEFINE' | 'define' | 'Define';
WAIT : 'WAIT' | 'wait' | 'Wait';
WITH : 'WITH' | 'with' | 'With';
AND : 'AND' | 'and' | 'And';
ACTOR : 'ACTOR' | 'actor' | 'Actor';
FOR : 'FOR' | 'for' | 'For';

RATE : 'RATE' | 'rate' | 'Rate';

VOLUME : 'VOLUME' | 'volume' | 'Volume';
FLOAT : '-'? (INT ('.' INT)? | ('.' INT));
fragment INT : [0-9]+ ;

LOOP : 'LOOP' | 'loop' | 'Loop';

VOICE : ('Male'[1-4]) | ('Female'[1-2]);

ID : [a-zA-Z0-9_-]+ ;
SLASH : '/' ;


FILE : [a-zA-Z0-9\/_-]+ FILE_POSTFIX;
fragment FILE_POSTFIX : '.' ('wav');

TEXT : '"' ~[\"]+ '"';


mode Comment ;
ANY : ~[#]+? -> channel(HIDDEN) ;
END_COMMENT : '#' -> mode(DEFAULT_MODE) ;
