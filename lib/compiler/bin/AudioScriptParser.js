// Generated from AudioScriptParser.g4 by ANTLR 4.5.1
// jshint ignore: start
var antlr4 = require('antlr4/index');
var AudioScriptParserListener = require('./AudioScriptParserListener').AudioScriptParserListener;
var grammarFileName = "AudioScriptParser.g4";

var serializedATN = ["\u0003\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd",
    "\u0003\u00161\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004\t",
    "\u0004\u0003\u0002\u0006\u0002\n\n\u0002\r\u0002\u000e\u0002\u000b\u0003",
    "\u0002\u0005\u0002\u000f\n\u0002\u0003\u0003\u0003\u0003\u0003\u0003",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0007\u0003\u0018\n",
    "\u0003\f\u0003\u000e\u0003\u001b\u000b\u0003\u0005\u0003\u001d\n\u0003",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003\u0003",
    "\u0003\u0003\u0003\u0003\u0003\u0003\u0005\u0003(\n\u0003\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0005\u0004/\n\u0004",
    "\u0003\u0004\u0002\u0002\u0005\u0002\u0004\u0006\u0002\u0003\u0003\u0002",
    "\u0013\u00146\u0002\t\u0003\u0002\u0002\u0002\u0004\'\u0003\u0002\u0002",
    "\u0002\u0006.\u0003\u0002\u0002\u0002\b\n\u0005\u0004\u0003\u0002\t",
    "\b\u0003\u0002\u0002\u0002\n\u000b\u0003\u0002\u0002\u0002\u000b\t\u0003",
    "\u0002\u0002\u0002\u000b\f\u0003\u0002\u0002\u0002\f\u000e\u0003\u0002",
    "\u0002\u0002\r\u000f\u0007\u0002\u0002\u0003\u000e\r\u0003\u0002\u0002",
    "\u0002\u000e\u000f\u0003\u0002\u0002\u0002\u000f\u0003\u0003\u0002\u0002",
    "\u0002\u0010\u0011\u0007\u0005\u0002\u0002\u0011\u0012\u0007\u0011\u0002",
    "\u0002\u0012\u001c\t\u0002\u0002\u0002\u0013\u0014\u0007\b\u0002\u0002",
    "\u0014\u0019\u0005\u0006\u0004\u0002\u0015\u0016\u0007\t\u0002\u0002",
    "\u0016\u0018\u0005\u0006\u0004\u0002\u0017\u0015\u0003\u0002\u0002\u0002",
    "\u0018\u001b\u0003\u0002\u0002\u0002\u0019\u0017\u0003\u0002\u0002\u0002",
    "\u0019\u001a\u0003\u0002\u0002\u0002\u001a\u001d\u0003\u0002\u0002\u0002",
    "\u001b\u0019\u0003\u0002\u0002\u0002\u001c\u0013\u0003\u0002\u0002\u0002",
    "\u001c\u001d\u0003\u0002\u0002\u0002\u001d(\u0003\u0002\u0002\u0002",
    "\u001e\u001f\u0007\u0007\u0002\u0002\u001f \u0007\u000b\u0002\u0002",
    " (\u0007\u0011\u0002\u0002!\"\u0007\u0006\u0002\u0002\"#\u0007\n\u0002",
    "\u0002#$\u0007\u0011\u0002\u0002$(\u0007\u0010\u0002\u0002%&\u0007\u0003",
    "\u0002\u0002&(\u0007\u0016\u0002\u0002\'\u0010\u0003\u0002\u0002\u0002",
    "\'\u001e\u0003\u0002\u0002\u0002\'!\u0003\u0002\u0002\u0002\'%\u0003",
    "\u0002\u0002\u0002(\u0005\u0003\u0002\u0002\u0002)*\u0007\r\u0002\u0002",
    "*/\u0007\u000e\u0002\u0002+/\u0007\u000f\u0002\u0002,-\u0007\f\u0002",
    "\u0002-/\u0007\u000e\u0002\u0002.)\u0003\u0002\u0002\u0002.+\u0003\u0002",
    "\u0002\u0002.,\u0003\u0002\u0002\u0002/\u0007\u0003\u0002\u0002\u0002",
    "\b\u000b\u000e\u0019\u001c\'."].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

var sharedContextCache = new antlr4.PredictionContextCache();

var literalNames = [ 'null', 'null', 'null', 'null', 'null', 'null', 'null', 
                     'null', 'null', 'null', 'null', 'null', 'null', 'null', 
                     'null', 'null', "'/'" ];

var symbolicNames = [ 'null', "COMMENT", "WS", "PROD_COMMAND", "DEFINE", 
                      "WAIT", "WITH", "AND", "ACTOR", "FOR", "RATE", "VOLUME", 
                      "FLOAT", "LOOP", "VOICE", "ID", "SLASH", "FILE", "TEXT", 
                      "ANY", "END_COMMENT" ];

var ruleNames =  [ "document", "element", "modification" ];

function AudioScriptParser (input) {
	antlr4.Parser.call(this, input);
    this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
    this.ruleNames = ruleNames;
    this.literalNames = literalNames;
    this.symbolicNames = symbolicNames;
    return this;
}

AudioScriptParser.prototype = Object.create(antlr4.Parser.prototype);
AudioScriptParser.prototype.constructor = AudioScriptParser;

Object.defineProperty(AudioScriptParser.prototype, "atn", {
	get : function() {
		return atn;
	}
});

AudioScriptParser.EOF = antlr4.Token.EOF;
AudioScriptParser.COMMENT = 1;
AudioScriptParser.WS = 2;
AudioScriptParser.PROD_COMMAND = 3;
AudioScriptParser.DEFINE = 4;
AudioScriptParser.WAIT = 5;
AudioScriptParser.WITH = 6;
AudioScriptParser.AND = 7;
AudioScriptParser.ACTOR = 8;
AudioScriptParser.FOR = 9;
AudioScriptParser.RATE = 10;
AudioScriptParser.VOLUME = 11;
AudioScriptParser.FLOAT = 12;
AudioScriptParser.LOOP = 13;
AudioScriptParser.VOICE = 14;
AudioScriptParser.ID = 15;
AudioScriptParser.SLASH = 16;
AudioScriptParser.FILE = 17;
AudioScriptParser.TEXT = 18;
AudioScriptParser.ANY = 19;
AudioScriptParser.END_COMMENT = 20;

AudioScriptParser.RULE_document = 0;
AudioScriptParser.RULE_element = 1;
AudioScriptParser.RULE_modification = 2;

function DocumentContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = AudioScriptParser.RULE_document;
    return this;
}

DocumentContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
DocumentContext.prototype.constructor = DocumentContext;

DocumentContext.prototype.element = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ElementContext);
    } else {
        return this.getTypedRuleContext(ElementContext,i);
    }
};

DocumentContext.prototype.EOF = function() {
    return this.getToken(AudioScriptParser.EOF, 0);
};

DocumentContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterDocument(this);
	}
};

DocumentContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitDocument(this);
	}
};




AudioScriptParser.DocumentContext = DocumentContext;

AudioScriptParser.prototype.document = function() {

    var localctx = new DocumentContext(this, this._ctx, this.state);
    this.enterRule(localctx, 0, AudioScriptParser.RULE_document);
    var _la = 0; // Token type
    try {
        this.enterOuterAlt(localctx, 1);
        this.state = 7; 
        this._errHandler.sync(this);
        _la = this._input.LA(1);
        do {
            this.state = 6;
            this.element();
            this.state = 9; 
            this._errHandler.sync(this);
            _la = this._input.LA(1);
        } while((((_la) & ~0x1f) == 0 && ((1 << _la) & ((1 << AudioScriptParser.COMMENT) | (1 << AudioScriptParser.PROD_COMMAND) | (1 << AudioScriptParser.DEFINE) | (1 << AudioScriptParser.WAIT))) !== 0));
        this.state = 12;
        var la_ = this._interp.adaptivePredict(this._input,1,this._ctx);
        if(la_===1) {
            this.state = 11;
            this.match(AudioScriptParser.EOF);

        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ElementContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = AudioScriptParser.RULE_element;
    return this;
}

ElementContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ElementContext.prototype.constructor = ElementContext;


 
ElementContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
};


function CommentContext(parser, ctx) {
	ElementContext.call(this, parser);
    ElementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

CommentContext.prototype = Object.create(ElementContext.prototype);
CommentContext.prototype.constructor = CommentContext;

AudioScriptParser.CommentContext = CommentContext;

CommentContext.prototype.COMMENT = function() {
    return this.getToken(AudioScriptParser.COMMENT, 0);
};

CommentContext.prototype.END_COMMENT = function() {
    return this.getToken(AudioScriptParser.END_COMMENT, 0);
};
CommentContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterComment(this);
	}
};

CommentContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitComment(this);
	}
};


function DefineContext(parser, ctx) {
	ElementContext.call(this, parser);
    this.id = null; // Token;
    this.voice = null; // Token;
    ElementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

DefineContext.prototype = Object.create(ElementContext.prototype);
DefineContext.prototype.constructor = DefineContext;

AudioScriptParser.DefineContext = DefineContext;

DefineContext.prototype.DEFINE = function() {
    return this.getToken(AudioScriptParser.DEFINE, 0);
};

DefineContext.prototype.ACTOR = function() {
    return this.getToken(AudioScriptParser.ACTOR, 0);
};

DefineContext.prototype.ID = function() {
    return this.getToken(AudioScriptParser.ID, 0);
};

DefineContext.prototype.VOICE = function() {
    return this.getToken(AudioScriptParser.VOICE, 0);
};
DefineContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterDefine(this);
	}
};

DefineContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitDefine(this);
	}
};


function ProduceContext(parser, ctx) {
	ElementContext.call(this, parser);
    this.command = null; // Token;
    this.id = null; // Token;
    this.file = null; // Token;
    this._modification = null; // ModificationContext;
    this.mods = []; // of ModificationContexts;
    ElementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

ProduceContext.prototype = Object.create(ElementContext.prototype);
ProduceContext.prototype.constructor = ProduceContext;

AudioScriptParser.ProduceContext = ProduceContext;

ProduceContext.prototype.PROD_COMMAND = function() {
    return this.getToken(AudioScriptParser.PROD_COMMAND, 0);
};

ProduceContext.prototype.ID = function() {
    return this.getToken(AudioScriptParser.ID, 0);
};

ProduceContext.prototype.FILE = function() {
    return this.getToken(AudioScriptParser.FILE, 0);
};

ProduceContext.prototype.TEXT = function() {
    return this.getToken(AudioScriptParser.TEXT, 0);
};

ProduceContext.prototype.WITH = function() {
    return this.getToken(AudioScriptParser.WITH, 0);
};

ProduceContext.prototype.modification = function(i) {
    if(i===undefined) {
        i = null;
    }
    if(i===null) {
        return this.getTypedRuleContexts(ModificationContext);
    } else {
        return this.getTypedRuleContext(ModificationContext,i);
    }
};

ProduceContext.prototype.AND = function(i) {
	if(i===undefined) {
		i = null;
	}
    if(i===null) {
        return this.getTokens(AudioScriptParser.AND);
    } else {
        return this.getToken(AudioScriptParser.AND, i);
    }
};

ProduceContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterProduce(this);
	}
};

ProduceContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitProduce(this);
	}
};


function CloseContext(parser, ctx) {
	ElementContext.call(this, parser);
    this.id = null; // Token;
    ElementContext.prototype.copyFrom.call(this, ctx);
    return this;
}

CloseContext.prototype = Object.create(ElementContext.prototype);
CloseContext.prototype.constructor = CloseContext;

AudioScriptParser.CloseContext = CloseContext;

CloseContext.prototype.WAIT = function() {
    return this.getToken(AudioScriptParser.WAIT, 0);
};

CloseContext.prototype.FOR = function() {
    return this.getToken(AudioScriptParser.FOR, 0);
};

CloseContext.prototype.ID = function() {
    return this.getToken(AudioScriptParser.ID, 0);
};
CloseContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterClose(this);
	}
};

CloseContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitClose(this);
	}
};



AudioScriptParser.ElementContext = ElementContext;

AudioScriptParser.prototype.element = function() {

    var localctx = new ElementContext(this, this._ctx, this.state);
    this.enterRule(localctx, 2, AudioScriptParser.RULE_element);
    var _la = 0; // Token type
    try {
        this.state = 37;
        switch(this._input.LA(1)) {
        case AudioScriptParser.PROD_COMMAND:
            localctx = new ProduceContext(this, localctx);
            this.enterOuterAlt(localctx, 1);
            this.state = 14;
            localctx.command = this.match(AudioScriptParser.PROD_COMMAND);
            this.state = 15;
            localctx.id = this.match(AudioScriptParser.ID);
            this.state = 16;
            localctx.file = this._input.LT(1);
            _la = this._input.LA(1);
            if(!(_la===AudioScriptParser.FILE || _la===AudioScriptParser.TEXT)) {
                localctx.file = this._errHandler.recoverInline(this);
            }
            else {
                this.consume();
            }
            this.state = 26;
            _la = this._input.LA(1);
            if(_la===AudioScriptParser.WITH) {
                this.state = 17;
                this.match(AudioScriptParser.WITH);
                this.state = 18;
                localctx._modification = this.modification();
                localctx.mods.push(localctx._modification);
                this.state = 23;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while(_la===AudioScriptParser.AND) {
                    this.state = 19;
                    this.match(AudioScriptParser.AND);
                    this.state = 20;
                    localctx._modification = this.modification();
                    localctx.mods.push(localctx._modification);
                    this.state = 25;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }

            break;
        case AudioScriptParser.WAIT:
            localctx = new CloseContext(this, localctx);
            this.enterOuterAlt(localctx, 2);
            this.state = 28;
            this.match(AudioScriptParser.WAIT);
            this.state = 29;
            this.match(AudioScriptParser.FOR);
            this.state = 30;
            localctx.id = this.match(AudioScriptParser.ID);
            break;
        case AudioScriptParser.DEFINE:
            localctx = new DefineContext(this, localctx);
            this.enterOuterAlt(localctx, 3);
            this.state = 31;
            this.match(AudioScriptParser.DEFINE);
            this.state = 32;
            this.match(AudioScriptParser.ACTOR);
            this.state = 33;
            localctx.id = this.match(AudioScriptParser.ID);
            this.state = 34;
            localctx.voice = this.match(AudioScriptParser.VOICE);
            break;
        case AudioScriptParser.COMMENT:
            localctx = new CommentContext(this, localctx);
            this.enterOuterAlt(localctx, 4);
            this.state = 35;
            this.match(AudioScriptParser.COMMENT);
            this.state = 36;
            this.match(AudioScriptParser.END_COMMENT);
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};

function ModificationContext(parser, parent, invokingState) {
	if(parent===undefined) {
	    parent = null;
	}
	if(invokingState===undefined || invokingState===null) {
		invokingState = -1;
	}
	antlr4.ParserRuleContext.call(this, parent, invokingState);
    this.parser = parser;
    this.ruleIndex = AudioScriptParser.RULE_modification;
    this.mod = null
    return this;
}

ModificationContext.prototype = Object.create(antlr4.ParserRuleContext.prototype);
ModificationContext.prototype.constructor = ModificationContext;


 
ModificationContext.prototype.copyFrom = function(ctx) {
    antlr4.ParserRuleContext.prototype.copyFrom.call(this, ctx);
    this.mod = ctx.mod;
};


function LoopContext(parser, ctx) {
	ModificationContext.call(this, parser);
    ModificationContext.prototype.copyFrom.call(this, ctx);
    return this;
}

LoopContext.prototype = Object.create(ModificationContext.prototype);
LoopContext.prototype.constructor = LoopContext;

AudioScriptParser.LoopContext = LoopContext;

LoopContext.prototype.LOOP = function() {
    return this.getToken(AudioScriptParser.LOOP, 0);
};
LoopContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterLoop(this);
	}
};

LoopContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitLoop(this);
	}
};


function VolumeContext(parser, ctx) {
	ModificationContext.call(this, parser);
    this.value = null; // Token;
    ModificationContext.prototype.copyFrom.call(this, ctx);
    return this;
}

VolumeContext.prototype = Object.create(ModificationContext.prototype);
VolumeContext.prototype.constructor = VolumeContext;

AudioScriptParser.VolumeContext = VolumeContext;

VolumeContext.prototype.VOLUME = function() {
    return this.getToken(AudioScriptParser.VOLUME, 0);
};

VolumeContext.prototype.FLOAT = function() {
    return this.getToken(AudioScriptParser.FLOAT, 0);
};
VolumeContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterVolume(this);
	}
};

VolumeContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitVolume(this);
	}
};


function RateContext(parser, ctx) {
	ModificationContext.call(this, parser);
    this.value = null; // Token;
    ModificationContext.prototype.copyFrom.call(this, ctx);
    return this;
}

RateContext.prototype = Object.create(ModificationContext.prototype);
RateContext.prototype.constructor = RateContext;

AudioScriptParser.RateContext = RateContext;

RateContext.prototype.RATE = function() {
    return this.getToken(AudioScriptParser.RATE, 0);
};

RateContext.prototype.FLOAT = function() {
    return this.getToken(AudioScriptParser.FLOAT, 0);
};
RateContext.prototype.enterRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.enterRate(this);
	}
};

RateContext.prototype.exitRule = function(listener) {
    if(listener instanceof AudioScriptParserListener ) {
        listener.exitRate(this);
	}
};



AudioScriptParser.ModificationContext = ModificationContext;

AudioScriptParser.prototype.modification = function() {

    var localctx = new ModificationContext(this, this._ctx, this.state);
    this.enterRule(localctx, 4, AudioScriptParser.RULE_modification);
    try {
        this.state = 44;
        switch(this._input.LA(1)) {
        case AudioScriptParser.VOLUME:
            localctx = new VolumeContext(this, localctx);
            this.enterOuterAlt(localctx, 1);
            this.state = 39;
            this.match(AudioScriptParser.VOLUME);
            this.state = 40;
            localctx.value = this.match(AudioScriptParser.FLOAT);
            break;
        case AudioScriptParser.LOOP:
            localctx = new LoopContext(this, localctx);
            this.enterOuterAlt(localctx, 2);
            this.state = 41;
            this.match(AudioScriptParser.LOOP);
            break;
        case AudioScriptParser.RATE:
            localctx = new RateContext(this, localctx);
            this.enterOuterAlt(localctx, 3);
            this.state = 42;
            this.match(AudioScriptParser.RATE);
            this.state = 43;
            localctx.value = this.match(AudioScriptParser.FLOAT);
            break;
        default:
            throw new antlr4.error.NoViableAltException(this);
        }
    } catch (re) {
    	if(re instanceof antlr4.error.RecognitionException) {
	        localctx.exception = re;
	        this._errHandler.reportError(this, re);
	        this._errHandler.recover(this, re);
	    } else {
	    	throw re;
	    }
    } finally {
        this.exitRule();
    }
    return localctx;
};


exports.AudioScriptParser = AudioScriptParser;
